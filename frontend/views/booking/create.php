<?php

/* @var $this yii\web\View */
/* @var $model Booking */

use common\models\Booking;
use dosamigos\datepicker\DatePicker;
use yii\bootstrap4\ActiveForm;
use yii\helpers\Html;

$this->title = "Забронировать комнату №$model->room_id"
?>

<div class="site-index">
    <div class="jumbotron text-center bg-transparent">
        <h1 class="display-4"><?= $this->title ?></h1>
    </div>
    <div class="body-content">
        <?php if ($model->hasErrors()): ?>
            <div class="error-summary">
                <div class="alert alert-success" role="alert">
                    <h4 class="alert-heading">Ошибки:</h4>
                    <?= Html::ul($model->getFirstErrors(), [
                        'class' => 'mb-3'
                    ]) ?>
                </div>
            </div>
        <?php endif; ?>

        <div class="row">

            <?php $form = ActiveForm::begin([
                'enableClientValidation'=>false
//                'action' => ['room/book', 'id' => $model->room_id]
            ]) ?>
                <?= $form->field($model, 'room_id')->hiddenInput()->label(false) ?>
                <?= $form->field($model, 'name') ?>
                <?= $form->field($model, 'email') ?>
                <?= $form->field($model, 'started_at')->widget(
                    DatePicker::class, [
                    'inline' => true,
                    // modify template for custom rendering
                    'template' => '<div class="well well-sm" style="background-color: #fff; width:250px">{input}</div>',
                    'clientOptions' => [
                        'autoclose' => true,
                        'format' => 'dd-mm-yyyy'
                    ]
                ]) ?>
                <?= $form->field($model, 'ended_at')->widget(
                    DatePicker::class, [
                    'inline' => true,
                    // modify template for custom rendering
                    'template' => '<div class="well well-sm" style="background-color: #fff; width:250px">{input}</div>',
                    'clientOptions' => [
                        'autoclose' => true,
                        'format' => 'dd-mm-yyyy'
                    ]
                ]) ?>
                <?= Html::submitButton('Отправить', ['class' => 'btn btn-primary']) ?>
            <?php ActiveForm::end() ?>

        </div>
    </div>
</div>