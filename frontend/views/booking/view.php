<?php

/* @var $this yii\web\View */
/* @var $booking Booking */

use frontend\models\Booking;
use yii\helpers\Html;
use yii\helpers\Url;

$this->title = 'Детали бронирования';
?>
<div class="site-index">

    <div class="jumbotron text-center bg-transparent">
        <h1 class="display-4">Благодарим Вас за выбор нашего отеля</h1>

        <div class="card mb-3">
            <div class="card-header text-white bg-primary">Детали бронирования</div>
            <div class="card-body">
                <?php foreach ($booking->getAttributes(except: ['created_at', 'updated_at', 'status', 'room_id']) as $key => $attribute): ?>
                    <div class="row border">
                        <div class="col-lg-6 text-left"><?= $booking->getAttributeLabel($key) ?></div>
                        <div class="col-lg-6 text-right"><?= Html::encode($attribute) ?></div>
                    </div>
                <?php endforeach; ?>
                <?php foreach ($booking->room->getAttributes(except: ['id', 'category_id', 'status', 'is_available']) as $key => $attribute): ?>
                    <div class="row border">
                        <div class="col-lg-6 text-left"><?= $booking->room->getAttributeLabel($key) ?></div>
                        <div class="col-lg-6 text-right"><?= Html::encode($attribute) ?></div>
                    </div>
                <?php endforeach; ?>
                <?php foreach ($booking->room->category->getAttributes(except: ['id', 'sort_order', 'status']) as $key => $attribute): ?>
                    <div class="row border">
                        <div class="col-lg-6 text-left"><?= $booking->room->category->getAttributeLabel($key) ?></div>
                        <div class="col-lg-6 text-right"><?= Html::encode($attribute) ?></div>
                    </div>
                <?php endforeach; ?>
            </div>
        </div>

        <p><a class="btn btn-lg btn-success" href="<?= Url::to(['/site/index'])?>">Перейти на главную</a></p>
    </div>
</div>
