<?php

/* @var $this yii\web\View */
/* @var $dataProvider Category[] */
/* @var $searchModel CategorySearch */

use frontend\models\Category;
use frontend\models\search\CategorySearch;
use yii\bootstrap4\ActiveForm;
use yii\bootstrap4\Html;
use yii\helpers\Url;

$this->title = 'Список категорий';
?>

<div class="site-index">
    <div class="jumbotron text-center bg-transparent">
        <h1 class="display-4"><?= $this->title ?></h1>
    </div>

    <div class="body-content">
        <div class="row">
            <div class="post-search">
                <?php $form = ActiveForm::begin([
                    'action' => ['index'],
                    'method' => 'get',
                    'enableClientValidation'=>false
                ]); ?>

                <?= $form->field($searchModel, 'name') ?>

                <div class="form-group">
                    <?= Html::submitButton('Искать', ['class' => 'btn btn-primary']) ?>
                    <?= Html::a('Сбросить', Url::to(['/category/index']) , ['class' => 'btn btn-default']) ?>
                </div>

                <?php ActiveForm::end(); ?>
            </div>
        </div>
        <div class="row">

            <?php foreach ($dataProvider as $category): ?>
                <div class="col-lg-4">
                    <h2><?= $category->name ?></h2>
                    <h5>Свободных номеров (<?= count($category->rooms) ?? 0 ?>)</h5>
                    <p>
                        <a href="<?= Url::to(['category/view', 'id' => $category->id]) ?>" class="btn btn-outline-secondary">
                            Смотреть категорию "<?= $category->name ?>"
                        </a>
                    </p>
                </div>
            <?php endforeach; ?>

        </div>
    </div>
</div>