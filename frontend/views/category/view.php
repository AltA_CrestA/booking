<?php

/* @var $this yii\web\View */
/* @var $category Category */

use frontend\models\Category;
use yii\helpers\Url;

$this->title = "Категория $category->name"
?>

<div class="site-index">
    <div class="jumbotron text-center bg-transparent">
        <h1 class="display-4"><?= $this->title ?></h1>
    </div>
    <div class="body-content">
        <div class="row">

            <?php foreach ($category->rooms as $room): ?>
                <div class="col-lg-4">
                    <h2>Комната <?= $room->number ?></h2>
                    <p>
                        <a href="<?= Url::to(['booking/create', 'id' => $room->id]) ?>" class="btn btn-outline-secondary">
                            Забронировать комнату "<?= $room->number ?>"
                        </a>
                    </p>
                </div>
            <?php endforeach; ?>

        </div>
    </div>
</div>