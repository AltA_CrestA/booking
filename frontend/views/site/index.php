<?php

/* @var $this yii\web\View */

use yii\helpers\Url;

$this->title = 'My Yii Application';
?>
<div class="site-index">

    <div class="jumbotron text-center bg-transparent">
        <h1 class="display-4">Booking</h1>

        <p><a class="btn btn-lg btn-success" href="<?= Url::to(['category/index'])?>">Перейти в категории</a></p>
    </div>
</div>
