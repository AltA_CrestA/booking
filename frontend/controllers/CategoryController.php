<?php

namespace frontend\controllers;

use frontend\models\Category;
use common\models\Status;
use common\services\CategoryService;
use frontend\models\search\CategorySearch;
use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

class CategoryController extends Controller
{
    public function __construct(
        $id, $module,
        private CategoryService $categoryService
    )
    {
        parent::__construct($id, $module);
    }

//    public function actionIndex(): string
//    {
//        $categoryList = Category::find()
//            ->with('rooms')
//            ->where(['status' => Status::ACTIVE])
//            ->orderBy('sort_order')
//            ->all();
//
//        return $this->render('index', [
//            'categoryList' => $categoryList
//        ]);
//    }

    public function actionIndex(): string
    {
        $searchModel = new CategorySearch();
        $dataProvider = $searchModel
            ->search(Yii::$app->request->get())
            ->query
            ->all();

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
        ]);
    }

    /**
     * @throws NotFoundHttpException
     */
    public function actionView(int $id): string
    {
        $category = $this->categoryService->getCategory($id, 'rooms');

        return $this->render('view', [
            'category' => $category
        ]);
    }
}