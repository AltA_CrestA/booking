<?php

namespace frontend\controllers;

use common\services\BookingService;
use frontend\models\Booking;
use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\Response;

class BookingController extends Controller
{
    public function __construct(
        $id, $module,
        private BookingService $bookingService
    )
    {
        parent::__construct($id, $module);
    }

    public function actionCreate(int $id): Response|string
    {
        $model = new Booking([
            'room_id' => $id
        ]);

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $model->save();

            return $this->redirect(['/booking/view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model
        ]);
    }

    /**
     * @throws NotFoundHttpException
     */
    public function actionView(int $id): string
    {
        $booking = $this->bookingService->getBooking($id, ['room', 'room.category']);

        return $this->render('view', [
            'booking' => $booking
        ]);
    }
}