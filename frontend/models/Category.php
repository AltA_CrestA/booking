<?php

namespace frontend\models;

use common\models\Room;
use common\models\Status;
use yii\db\ActiveQuery;

class Category extends \common\models\Category
{
    /**
     * @return ActiveQuery
     */
    public function getRooms(): ActiveQuery
    {
        return $this->hasMany(Room::class, ['category_id' => 'id'])
            ->andWhere([
                'room.status' => Status::ACTIVE,
                'room.is_available' => true
            ]);
    }
}