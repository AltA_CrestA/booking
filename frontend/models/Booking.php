<?php

namespace frontend\models;

use common\models\Status;
use DateTime;
use yii\helpers\ArrayHelper;

class Booking extends \common\models\Booking
{
    public function rules(): array
    {
        return ArrayHelper::merge(parent::rules(), [
            [['room_id', 'email', 'name', 'started_at', 'ended_at'], 'required'],

            ['room_id', 'validateRoom'],

            ['started_at', 'validateStartedAt'],

            ['ended_at', 'validateEndedAt'],
        ]);
    }

    public function validateRoom()
    {
        if (!$this->room->is_available) {
            $this->addError("Комната №{$this->room->number} уже забронирована");
        }
    }

    public function validateStartedAt()
    {
        $this->started_at = (new DateTime($this->started_at))->format('Y-m-d');
        $now = (new DateTime("now"))->format('Y-m-d');
        if ($this->started_at < $now) {
            $this->addError('started_at', 'Дата заезда должна быть больше настоящего времени');
        }
    }

    public function validateEndedAt()
    {
        $this->ended_at = (new DateTime($this->ended_at))->format('Y-m-d');

        if ($this->ended_at < $this->started_at) {
            $this->addError('ended_at', 'Дата выезда должна быть больше, либо равна дате заезда');
        }
    }

    public function beforeSave($insert): bool
    {
        if (!parent::beforeSave($insert)) {
            return false;
        }

        if ($insert) {
            $this->status = Status::ACTIVE;

            if ($this->started_at == (new DateTime())->format('Y-m-d')) {
                $this->room->is_available = false;
                $this->room->save();
            }
        }
        return true;
    }
}