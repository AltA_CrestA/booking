<?php

namespace frontend\models\search;

use common\models\Status;
use frontend\models\Category;
use yii\data\ActiveDataProvider;

class CategorySearch extends Category
{
    public function search($params): ActiveDataProvider
    {
        $query = Category::find()
            ->with('rooms')
            ->where(['status' => Status::ACTIVE])
            ->orderBy('sort_order');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!$this->load($params)) {
            return $dataProvider;
        }

        $query->andFilterWhere(['like', 'name', $this->name]);

        return $dataProvider;
    }
}