<?php

use yii\db\Migration;

class m210824_100418_create_booking_table extends Migration
{
    public function safeUp()
    {
        $this->createTable('{{%booking}}', [
            'id'            => $this->primaryKey(),
            'room_id'       => $this->integer()->notNull(),
            'name'          => $this->string()->notNull(),
            'email'         => $this->string()->notNull(),
            'status'        => $this->integer()->defaultValue(0)->notNull()->unsigned(),
            'started_at'    => $this->date()->notNull(),
            'ended_at'      => $this->date()->notNull(),
            'created_at'    => $this->timestamp()->notNull(),
            'updated_at'    => $this->timestamp()->notNull()
        ]);

        $this->createIndex(
            '{{%idx-booking-room_id}}',
            '{{%booking}}',
            'room_id'
        );

        $this->addForeignKey(
            '{{%fk-booking-room_id}}',
            '{{%booking}}',
            'room_id',
            '{{%room}}',
            'id',
            'RESTRICT',
            'CASCADE'
        );
    }

    public function safeDown()
    {
        $this->dropTable('{{%booking}}');
    }
}
