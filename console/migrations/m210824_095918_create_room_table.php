<?php

use yii\db\Migration;

class m210824_095918_create_room_table extends Migration
{
    public function safeUp()
    {
        $this->createTable('{{%room}}', [
            'id'            => $this->primaryKey(),
            'number'        => $this->integer()->notNull()->unique()->unsigned(),
            'category_id'   => $this->integer()->notNull(),
            'is_available'  => $this->boolean()->defaultValue(true)->notNull(),
            'status'        => $this->integer()->defaultValue(0)->notNull()->unsigned()
        ]);

        $this->createIndex(
            '{{%idx-room-category_id}}',
            '{{%room}}',
            'category_id'
        );

        $this->addForeignKey(
            '{{%fk-room-category_id}}',
            '{{%room}}',
            'category_id',
            '{{%category}}',
            'id',
            'RESTRICT',
            'CASCADE'
        );
    }

    public function safeDown()
    {
        $this->dropTable('{{%room}}');
    }
}
