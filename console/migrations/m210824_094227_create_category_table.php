<?php

use yii\db\Migration;

class m210824_094227_create_category_table extends Migration
{
    public function safeUp()
    {
        $this->createTable('{{%category}}', [
            'id'            => $this->primaryKey(),
            'name'          => $this->string()->notNull()->unique(),
            'sort_order'    => $this->integer()->unsigned()->null(),
            'status'        => $this->integer()->defaultValue(0)->notNull()->unsigned()
        ]);
    }

    public function safeDown()
    {
        $this->dropTable('{{%category}}');
    }
}
