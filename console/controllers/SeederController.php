<?php

namespace console\controllers;

use common\models\Category;
use common\models\Room;
use common\models\Status;
use Yii;
use yii\console\Controller;
use yii\console\ExitCode;
use yii\db\Exception;

class SeederController extends Controller
{
    private array $roomCategoryData = [
        ['name' => 'Одноместный', 'sort_order' => 10, 'status' => Status::ACTIVE],
        ['name' => 'Двуместный', 'sort_order' => 20, 'status' => Status::ACTIVE],
        ['name' => 'Люкс', 'sort_order' => 30, 'status' => Status::ACTIVE],
        ['name' => 'Де-Люкс', 'sort_order' => 40, 'status' => Status::ACTIVE],
    ];
    private array $roomItemData = [
        ['number' => 301, 'category_id' => 1, 'status' => Status::ACTIVE],
        ['number' => 401, 'category_id' => 1, 'status' => Status::ACTIVE],

        ['number' => 302, 'category_id' => 2, 'status' => Status::ACTIVE],
        ['number' => 303, 'category_id' => 2, 'status' => Status::ACTIVE],
        ['number' => 402, 'category_id' => 2, 'status' => Status::ACTIVE],
        ['number' => 403, 'category_id' => 2, 'status' => Status::ACTIVE],

        ['number' => 304, 'category_id' => 3, 'status' => Status::ACTIVE],
        ['number' => 404, 'category_id' => 3, 'status' => Status::ACTIVE],
        ['number' => 305, 'category_id' => 3, 'status' => Status::ACTIVE],

        ['number' => 405, 'category_id' => 4, 'status' => Status::ACTIVE],
        ['number' => 306, 'category_id' => 4, 'status' => Status::ACTIVE],
        ['number' => 406, 'category_id' => 4, 'status' => Status::ACTIVE],
        ['number' => 307, 'category_id' => 4, 'status' => Status::ACTIVE],
        ['number' => 407, 'category_id' => 4, 'status' => Status::ACTIVE],
    ];

    /**
     * @throws Exception
     */
    public function actionIndex(): int
    {
        $transaction = Yii::$app->db->beginTransaction();

        try {
            foreach ($this->getData() as $data) {
                $this->createModels($data['data'], $data['class']);
            }

            $transaction->commit();
        } catch (Exception $th) {
            $transaction->rollBack();
            throw new Exception($th->getMessage());
        }

        return ExitCode::OK;
    }

    private function createModels(array $dataArray, string $className)
    {
        foreach ($dataArray as $data) {
            $model = new $className($data);
            $model->save();
        }
    }

    private function getData(): array
    {
        return [
            [
                'data' => $this->roomCategoryData,
                'class' => Category::class
            ],
            [
                'data' => $this->roomItemData,
                'class' => Room::class
            ]
        ];
    }
}