<?php

namespace common\models;

use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\db\Expression;

/**
 * @property int $room_id
 * @property string $name
 * @property string $email
 * @property string $status
 * @property string $started_at
 * @property string $ended_at
 * @property string $created_at
 * @property string $updated_at
 *
 * @property Room $room
 */
class Booking extends ActiveRecord
{
    public static function tableName(): string
    {
        return '{{%booking}}';
    }

    public function behaviors(): array
    {
        return [
            [
                'class' => TimestampBehavior::class,
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new Expression('NOW()'),
            ],
        ];
    }

    public function rules(): array
    {
        return [
            ['room_id', 'integer'],
            ['room_id', 'exist', 'targetClass' => Room::class, 'targetAttribute' => 'id'],

            ['name', 'string', 'min' => 2, 'max' => 255],

            ['email', 'string'],
            ['email', 'email'],

            ['status', 'integer'],
            ['status', 'in', 'range' => Status::getStatusList()],
            ['status', 'default', 'value' => 0],

            ['started_at', 'string'],
            ['ended_at', 'string'],
            ['created_at', 'string'],
            ['updated_at', 'string'],
        ];
    }

    public function attributeLabels(): array
    {
        return [
            'id'            => 'Номер брони',
            'name'          => 'Имя',
            'email'         => 'E-mail',
            'started_at'    => 'Дата заезда',
            'ended_at'      => 'Дата выезда'
        ];
    }

    public function getRoom()
    {
        return $this->hasOne(Room::class, ['id' => 'room_id'])
            ->andWhere(['room.status' => Status::ACTIVE]);
    }
}