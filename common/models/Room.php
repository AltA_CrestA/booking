<?php

namespace common\models;

use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

/**
 * @property int $id
 * @property int $number
 * @property int $category_id
 * @property int $status
 * @property bool $is_available
 *
 * @property Category $category
 */
class Room extends ActiveRecord
{
    public static function tableName(): string
    {
        return '{{%room}}';
    }

    public function rules(): array
    {
        return [
            ['number', 'integer'],
            ['number', 'unique'],

            ['category_id', 'integer'],
            ['category_id', 'exist', 'targetClass' => Category::class, 'targetAttribute' => 'id'],

            ['status', 'integer'],
            ['status', 'in', 'range' => Status::getStatusList()],
            ['status', 'default', 'value' => 0]
        ];
    }

    public function attributeLabels(): array
    {
        return [
            'id' => 'ID',
            'number' => 'Номер комнаты',
            'status' => 'Статус'
        ];
    }

    public function getCategory(): ActiveQuery
    {
        return $this->hasOne(Category::class, ['id' => 'category_id']);
    }
}