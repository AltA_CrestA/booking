<?php

namespace common\models;

use yii\base\Model;

class Status extends Model
{
    public const IN_DRAFT = 0;
    public const ACTIVE = 10;
    public const REMOVED = 20;

    public static function getStatusList(): array
    {
        return [
            self::IN_DRAFT,
            self::ACTIVE,
            self::REMOVED
        ];
    }
}