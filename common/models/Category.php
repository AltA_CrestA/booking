<?php

namespace common\models;

use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

/**
 * @property int $id
 * @property string $name
 * @property int $sort_order
 * @property int $status
 *
 * @property Room[] $rooms
 */
class Category extends ActiveRecord
{
    public static function tableName(): string
    {
        return '{{%category}}';
    }

    public function rules(): array
    {
        return [
            ['name', 'string', 'min' => 2, 'max' => 255],
            ['name', 'unique'],

            ['sort_order', 'integer'],
            ['sort_order', 'default', 'value' => null],

            ['status', 'integer'],
            ['status', 'in', 'range' => Status::getStatusList()],
            ['status', 'default', 'value' => 0]
        ];
    }

    public function attributeLabels(): array
    {
        return [
            'id' => 'ID',
            'name' => 'Категория',
            'status' => 'Статус'
        ];
    }

    /**
     * @return ActiveQuery
     */
    public function getRooms(): ActiveQuery
    {
        return $this->hasMany(Room::class, ['category_id' => 'id']);
    }
}