<?php

namespace common\services;

use frontend\models\Category;
use yii\web\NotFoundHttpException;

class CategoryService
{
    /**
     * @throws NotFoundHttpException
     */
    public function getCategory(int $id, array|string $with = null): ?Category
    {
        $query = Category::find();
        if ($with) {
            $query->with($with);
        }
        $query->andWhere(['id' => $id]);

        /** @var Category|null $model */
        $model = $query->one();
        if (!$model) throw new NotFoundHttpException('Категория не найдена');

        return $model;
    }
}