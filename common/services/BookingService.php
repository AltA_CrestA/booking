<?php

namespace common\services;

use common\models\Booking;
use yii\web\NotFoundHttpException;

class BookingService
{
    /**
     * @throws NotFoundHttpException
     */
    public function getBooking(int $id, array|string $with = null): ?Booking
    {
        $query = Booking::find();
        if ($with) {
            $query->with($with);
        }
        $query->andWhere(['id' => $id]);

        /** @var Booking|null $model */
        $model = $query->one();
        if (!$model) throw new NotFoundHttpException('Бронь не найдена');

        return $model;
    }
}